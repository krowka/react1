import React from "react";
import List from "../../components/Header/react1.js";


const Homepage = () => {
  return (
    <div>
      <h1>Moje ulubione dania:</h1>
      <List dish="Pierogi leniwe" />
      <List dish="Naleśniki" />
      <List dish="Gołąbki" />
      <List dish="Zupa buraczkowa" />
      <List dish="Ciastka kruche" />
    </div>
  );
};
export default Homepage;

