import React from "react";

const List = (props) => {
  return (
    <div>
      <li>{props.dish}</li>
    </div>
  );
};

// class List extends React.Component{
//     render(){
//         return(
//         <div>
//             <li>{this.props.dish}</li>
//         </div>)
//     }
// }
export default List;
